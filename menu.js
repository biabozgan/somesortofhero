const readline = require('readline');
const chalk = require('chalk');
const lines = require('./dialog/textDialog');


function checkAns(ans) {
    console.log(ans);
    switch (ans) {
        case 'start': case 'about':
            runCommand(ans);
            break
        default:
            runCommand('unknown');
            break
    }
}

var runCommand = async function (command) {
    const ans = await askQuestion(lines[command]);
    // a.startBattle();
}

function askQuestion(query) {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    return new Promise(resolve => rl.question(query, ans => {
        rl.close();
        resolve(ans);
        checkAns(ans);
        // startCommand()
    }))
}

module.exports =  runCommand;