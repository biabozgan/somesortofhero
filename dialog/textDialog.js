let chalk = require('chalk');
let lines = {
    about: `You are on a whatever` + chalk.red(`BOOM`)+ chalk.green(`BOOM \n`),
    both_dead:'Both players are dead',
    critical_strike: 'Critical Strike.',
    damage: '(-{0} health points)',
    divider: '=========================',
    error: 'Error',
    health: 'Health',
    initial_stats: 'Initial Stats',
    miss: 'Miss',
    no_action: ' No action made.',
    no_effect: 'No effect.',
    over: 'Fight is over',
    resilience: 'Resilience.',
    round: 'Round',
    start: chalk.bold(`Hello! \n\n MENU:\n`) + chalk.red(`BOOM \n`),
    start_battle: 'Starting the battle',
    stats: 'Stats',
    won_match: ' won the match'
}

module.exports = lines;