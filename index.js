const Character = require('./blueprints/character');
const Arena = require('./blueprints/arena');
const stats = require('./stats');

var c = new Character('HERO', stats.hero);
var c2 = new Character('BAD GUY', stats.villain);
let a = new Arena(c, c2);


a.startBattle();
