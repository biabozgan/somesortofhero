const dialog = require('../dialog/textDialog');
const chalk = require('chalk');
class Arena {
    constructor(hero, villan) {
        this.hero = hero;
        this.villan = villan;
        this.queue = [];
        this.round = 1;
    }

    //Start the battle between the characters
    startBattle() {
        //Pick the first character that makes an attack
        if (this.getOrder() === dialog.error) return dialog.error;
        this.printMessage(dialog.initial_stats + ':');
        //Show the characters stats
        this.hero.printStats();
        this.villan.printStats();
        this.printMessage(dialog.divider + '\n');
        setTimeout(this.fight.bind(this), 5 * 1000);
        this.printMessage(dialog.start_battle + '...\n');
        setTimeout(() => console.log(1), 3 * 1000);
        setTimeout(() => console.log(2), 2 * 1000);
        setTimeout(() => console.log(3), 1 * 1000);
    }

    //Pick the starting order depending on speed and luck
    getOrder() {
        //Characters are not valid
        if (this.validCharacters() === false) return dialog.error;
        //The hero has more speed points
        if (this.hero.getSpeed() > this.villan.getSpeed()) {
            this.queue = [this.hero, this.villan];
            //Both characters have the same speed
        } else if (this.hero.getSpeed() === this.villan.getSpeed()) {
            //The hero is more lucky or same as lucky as the villan
            if (this.hero.getLuck() >= this.villan.getLuck()) {
                this.queue = [this.hero, this.villan];
                // The villan is more lucky
            } else {
                this.queue = [this.villan, this.hero];
            }
            //The villan has more speed points  
        } else {
            this.queue = [this.villan, this.hero];
        }
        return this.queue;
    }

    //Arena fight method
    fight() {
        // If the characters are figthing for 20 round then the fight is over
        if (this.round > 20) {
            this.printMessage(dialog.over);
            return;
        }
        let message ='';
        this.printMessage(chalk.bgCyan(dialog.round + ' ' + this.round));
        //Get the characters from the order queue
        let attacker = this.queue.shift();
        let defender = this.queue.shift();
        //The first character is tring to do an attack move
        let damage = attacker.callActiveSkill(defender.getDefence(),
            defender.getLuck());
        let tempHealth = defender.getHealth();
        //The second character is tring to do a passive move
        defender.callPassiveSkill(damage);
        //Prepare the order for the next round
        this.queue = [defender, attacker];
        //Show the damage made by the attacker
        message += defender.printNewStats(tempHealth, 'health');
        message += dialog.divider;
        this.printMessage(message);
        //Increase the round number
        this.round++;
        //Check if any of the characters are over
        let over = this.checkIfOver();
        if (over >= 0) {
            return over;
        }
        //Start a new round
        setTimeout(this.fight.bind(this), 2 * 1000);
    }

    /**
     * Check if the arena's characters have health < 0
     */
    checkIfOver() {
        // The hero health is < 0
        if (this.hero.getHealth() < 0 && this.villan.getHealth() > 0) {
            this.printMessage(chalk.bgBlue(this.villan.getName() + dialog.won_match));
            return 2;
        }
        // The villan health is < 0
        if (this.villan.getHealth() < 0 && this.hero.getHealth() > 0) {
            this.printMessage(chalk.bgBlue(this.hero.getName() + dialog.won_match));
            return 1;
        }
        //Both characters are dead
        if (this.villan.getHealth() < 0 && this.hero.getHealth() < 0) {
            this.printMessage(chalk.bgBlue(dialog.both_dead));
            return 0;
        }
        //Both characters are alive
        return -1;
    }

    printMessage(message) {
        console.log(message);
    }

    //Check if the character's object are valid
    validCharacters() {
        if (!this.hero) return false;
        if (!this.villan) return false;

        if (!this.hero.characterIsValid() ||
            !this.villan.characterIsValid()) {
            return false
        }
    }
}

module.exports = Arena;