let chalk = require('chalk');
let Skill = require('./skill');
let dialog = require('../dialog/textDialog');

class Character {

    //Create new character with all the stats(health, strength, defence, speed, luck)
    constructor(name, statsRulles) {
        this.name = name;
        this.stats = {};
        this.history = [];
        this.stats.health = this.getRandomStats(statsRulles.health);
        this.stats.strength = this.getRandomStats(statsRulles.strength);
        this.stats.defence = this.getRandomStats(statsRulles.defence);
        this.stats.speed = this.getRandomStats(statsRulles.speed);
        this.stats.luck = this.getRandomStats(statsRulles.luck);
        this.initializeSkills();
    }

    //Getters
    getHealth() { return this.stats.health };
    getStrength() { return this.stats.strength };
    getDefence() { return this.stats.defence };
    getSpeed() { return this.stats.speed };
    getLuck() { return this.stats.luck };
    getName() { return this.name };

    //Setters
    setHealth(health) { this.stats.health = health; }
    setSpeed(speed) { this.stats.speed = speed; }
    setLuck(luck) { this.stats.luck = luck; }
    setStrength(strength) { this.stats.strength = strength; }

    /**Get random stats(numbers) between given value
     * value: object with fields min and max 
     */
    getRandomStats(values) {
        let min = 0,
            max = 100;
        if (values) {
            min = values.min ? values.min : min;
            max = values.max ? values.max : max;
        }
        if (min > max) max = min + max - (min = max);
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    // Initialize the activeSkills and passiveSkills arrays with skills objects
    initializeSkills() {
        this.activeSkills = [new Skill('critStrike', 0.1, this.criticalStrike)];
        this.passiveSkills = [new Skill('resilience', 0.2, this.resilience, false)];
    }

    //Lower the character health with damage points
    lowerHealth(damage) {
        this.stats.health -= damage;
    }

    //Used to output the character stats in a clear way
    printStats() {
        console.log(chalk.bold.inverse(this.name + '\'s ' + dialog.stats.toUpperCase()));
        let statsString = '';
        for (const stat in this.stats) {
            statsString += chalk.underline(chalk.bold(stat) + ': ' + this.stats[stat] + ' | ');
        }
        console.log(statsString);
    }

    /**Used to return a mesage with  the evolution of the stats. Depending on the new 
     * value different background colors are used
     */
    printNewStats(temp, field) {
        let statsString = '';
        statsString += chalk.underline(chalk.bold(field) + ': ' +
            temp + ' -> ' + this.stats[field]);
        if (temp - this.stats[field] > 50) {
            statsString = chalk.bgRed(statsString);
        } else if (temp - this.stats[field] > 30) {
            statsString = chalk.bgRedBright(statsString);
        } else if (temp - this.stats[field] > 0) {
            statsString = chalk.bgYellow.gray(statsString);
        } else {
            statsString = chalk.bgGreen(statsString);
        }
        statsString = chalk.bold.inverse(this.name + '\'s ' + dialog.health.toUpperCase()) + ' ' +
            statsString + '\n';


        return statsString;
    }

    /**Used to calculate the chance that a character has for a attack move.
     * Depending on the chances that the skills have a character can make a 
     * move. If nothing gets picked then the character does not make any moves.
     * A character and also miss if the opponent is lucky.
     * !Some moves may require to not be used twice in a row. 
     */
    callActiveSkill(opDefence, opLuck) {
        let list = this.activeSkills;
        let rand = Math.random();
        let accumulatedChance = 0;
        let found = list.find(function (element) {
            accumulatedChance += element.chance;
            return accumulatedChance >= rand;
        })

        if (found) {
            //Skill can't be used twice in a row and is was already used in the last round
            if ((found.consecutive === false && this.history[this.history.length - 2] === found.id)) {
                console.log(this.name + ':' + dialog.no_action);
                this.history.push(dialog.no_action);
                return 0;
            }

            //Opponent was lucky and the character missed
            if (Math.random() < opLuck / 100) {
                console.log(this.name + ': ' + dialog.miss);
                this.history.push(dialog.miss);
                return 0;
            }

            //A skills was picked and it's going to be called
            this.history.push(found.id);
            return found.func(this, opDefence);
        } else {
            //No skill picked
            console.log(this.name + ':' + dialog.no_action);
            this.history.push(dialog.no_action);
            return 0;
        }
    }

    /**Used to calculate the chance that a character has for a defence move.
     * Depending on the chances that the skills have a character can make a 
     * move. If nothing gets picked then the character does not make any moves.
     * !Some moves may require to not be used twice in a row. 
     */
    callPassiveSkill(damage) {
        let list = this.passiveSkills;
        let rand = Math.random();
        let accumulatedChance = 0;
        let found = list.find(function (element) {
            accumulatedChance += element.chance;
            return accumulatedChance >= rand;
        })

        if (found) {
            //Skill can't be used twice in a row and is was already used in the last round
            if ((found.consecutive === false &&
                this.history[this.history.length - 2] === found.id)) {
                console.log(this.name + ':' + dialog.no_action);
                this.history.push(dialog.no_action);
                return 0;
            }
            //A skills was picked and it's going to be called
            this.history.push(found.id);
            return found.func(this, damage);
        } else {
            //No skill picked
            console.log(this.name + ': ' + dialog.no_action +
                dialog.damage.replace('{0}', damage));
            this.lowerHealth(damage);
            this.history.push(dialog.no_action);
        }
    }

    //Check if the character object has all the stats
    characterIsValid() {
        let response;
        if (this && typeof (this.getDefence()) === 'number' &&
            typeof (this.getHealth()) === 'number' &&
            typeof (this.getLuck()) === 'number' &&
            typeof (this.getName()) === 'string' &&
            typeof (this.getSpeed()) === 'number') {
            response = true;
        } else {
            response = false;
        }
        return response;
    }


    //***********Active skills***************//
    /**Character gets to hit twice. One hit generates damage equal to
     * character's strength minus opponent defence
     */
    criticalStrike(self, opDefence) {
        let damage = 0;
        /**If the character's strength is smaller the the opponent defence
         * then the damage made is 0 
         */
        if (opDefence > self.getStrength()) {
            console.log(self.name + ': ' + dialog.critical_strike +
                '. ' + dialog.no_effect);
            return 0;
        }
        damage += 2 * (self.getStrength() - opDefence);
        let rand = Math.random();
        /**
         * If lucky the character gets to hit once more so more damage is generated
         */
        if (rand < 0.01) {
            damage += self.getStrength() - opDefence;
        }
        console.log(self.name + ': ' + dialog.critical_strike);
        return damage;
    }
    //***********Active skills***************//

    //***********Passive skills***************//
    /**Character gets hurt with half the damage made by the opponent
     */
    resilience(self, damage) {
        if (damage < 0) return 0;
        damage = Math.floor(damage / 2);
        self.lowerHealth(damage);
        console.log(self.name + ': ' + dialog.resilience +
            dialog.damage.replace('{0}', damage));
        return damage;
    }
    //***********Passive skills***************//
}

module.exports = Character;