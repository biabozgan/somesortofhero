class Skill {
    constructor(id, chance, func, consecutive = true) {
        this.id = id;
        this.chance = chance;
        this.func = func;
        this.consecutive = consecutive;
    }
}

module.exports = Skill;