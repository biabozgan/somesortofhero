## Some Sort of Hero

Hello mortal,

Welcome to THE GATE. If you want to pass into the realm 
of the shadows you must first see your chances.

The realm of shadows is full of evil creatures that will try
to get the essence of your being on the first second you 
step your foot on the ground. So, if you think you're strong
and lucky enough you can try a simulate a battle before
going into certain death.

## Requirements
* Node 10
* Npm 6
## Instalation
Go into the project's root and run:
```javascript
npm install
```
## Usage
Go into the project's root and run:
```javascript
npm run game //Starts the simulation
npm test //Run unit tests and shows code coverage(cli)
npm testHTML //Run unit tests and generates html report
```

## Screenshots
![Simulation demo](screenshots/simulation.PNG)
A demo of a battle

![Damage types](screenshots/damage.png)
Output depending on the damage points

## Notes
* Character stats ranges are stored into ./stats.js file. You can
add more character types and change stats range.
* You can change skills chances in the Character class(initializeSkills method)

## Still in progress
* Code coverage for unit tests
* Add a small menu(start battle, about, change hero stats)