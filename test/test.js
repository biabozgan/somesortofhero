const chai = require('chai');
const { expect, assert } = require('chai');
const sinonChai = require('sinon-chai');
const sinon = require('sinon');
const Arena = require('../blueprints/arena');
const Character = require('../blueprints/character');
const stats = require('../stats');

chai.use(sinonChai);

describe('Test logging', function () {

    beforeEach(function () {
        sinon.spy(console, 'log');
    });

    afterEach(function () {
        console.log.restore();
    });

    describe('Test that the console.log is called correctly ', function () {
        it('should log to console when send message to printMessage function', function () {
            let arena = new Arena(null, null);
            arena.printMessage('Test');
            expect(console.log.calledOnce).to.be.true;
            expect(console.log.calledWith('Test')).to.be.true;
        });
        it('should log to console when print stats', function () {
            let character = new Character('test', stats.hero);
            character.printStats();
            expect(console.log.calledTwice).to.be.true;
        });
        it('should log to console when call active skill', function () {
            let character = new Character('test', stats.hero);
            character.callActiveSkill(10,0);
            expect(console.log.called).to.be.true;
        });
        it('should log to console when call passive skill', function () {
            let character = new Character('test', stats.hero);
            character.callPassiveSkill(10,0);
            expect(console.log.called).to.be.true;
        });
    });

});

describe('Test the arena class methods', function () {
    it('should return Error because characters are not valid', function () {
        let arena = new Arena(null, null);
        expect(arena.getOrder()).to.equal('Error');
    });
    it('should return the characters in order: first character and then the second', function () {
        let c1 = new Character('test1', stats.hero);
        let c2 = new Character('test2', stats.hero);
        c1.setSpeed(10);
        c2.setSpeed(0);
        let arena = new Arena(c1, c2);
        expect(arena.getOrder()).to.eql([c1, c2]);
    });
    it('should return the characters in order: second character and then the first', function () {
        let c1 = new Character('test1', stats.hero);
        let c2 = new Character('test2', stats.hero);
        c1.setSpeed(0);
        c2.setSpeed(10);
        let arena = new Arena(c1, c2);
        expect(arena.getOrder()).to.eql([c2, c1]);
    });
    it('should return the characters in order: first character and then the second', function () {
        let c1 = new Character('test1', stats.hero);
        let c2 = new Character('test2', stats.hero);
        c1.setSpeed(0);
        c2.setSpeed(0);
        c1.setLuck(10);
        c2.setLuck(10);
        let arena = new Arena(c1, c2);
        expect(arena.getOrder()).to.eql([c1, c2]);
    });
    it('should return the characters in order: second character and then the first', function () {
        let c1 = new Character('test1', stats.hero);
        let c2 = new Character('test2', stats.hero);
        c1.setSpeed(0);
        c2.setSpeed(0);
        c1.setLuck(0);
        c2.setLuck(10);
        let arena = new Arena(c1, c2);
        expect(arena.getOrder()).to.eql([c2, c1]);
    });



    it('should return false because characters are not valid(null character)', function () {
        let c1 = new Character('test1', stats.hero);
        let arena = new Arena(c1, null);
        expect(arena.validCharacters()).to.equal(false);
    });
    it('should return false because characters are not valid(character health is a letter)', function () {
        let c1 = new Character('test1', stats.hero);
        let c2 = new Character('test2', stats.hero);
        c1.setHealth('a');
        let arena = new Arena(c1, c2);
        expect(arena.validCharacters()).to.equal(false);
    });


    it('should return Error because characters are not valid(null character)', function () {
        let c1 = new Character('test1', stats.hero);
        let arena = new Arena(c1, null);
        expect(arena.startBattle()).to.equal('Error');
    });



    it('should return 2 because the second player won the match', function () {
        let c1 = new Character('test1', stats.hero);
        let c2 = new Character('test2', stats.hero);
        c1.setHealth(-1);
        c2.setHealth(10);
        let arena = new Arena(c1, c2);
        expect(arena.checkIfOver()).to.equal(2);
    });
    it('should return 1 because the first player won the match', function () {
        let c1 = new Character('test1', stats.hero);
        let c2 = new Character('test2', stats.hero);
        c1.setHealth(100);
        c2.setHealth(-2);
        let arena = new Arena(c1, c2);
        expect(arena.checkIfOver()).to.equal(1);
    });
    it('should return 0 because both players died', function () {
        let c1 = new Character('test1', stats.hero);
        let c2 = new Character('test2', stats.hero);
        c1.setHealth(-43);
        c2.setHealth(-2);
        let arena = new Arena(c1, c2);
        expect(arena.checkIfOver()).to.equal(0);
    });
    it('should return -1 because both players are still alive', function () {
        let c1 = new Character('test1', stats.hero);
        let c2 = new Character('test2', stats.hero);
        let arena = new Arena(c1, c2);
        expect(arena.checkIfOver()).to.equal(-1);
    });
});

describe('Test the character class methods', function () {
    it('should return false because, character is not valid(character health is a letter)', function () {
        let c = new Character('test', stats.hero);
        c.setHealth('a');
        expect(c.characterIsValid()).to.equal(false);
    });

    it('should return true because character is valid', function () {
        let c1 = new Character('test1', stats.hero);
        expect(c1.characterIsValid()).to.equal(true);
    });


    it('should return a number between the given numbers(4,200)', function () {
        let character = new Character('test2', stats.hero);
        let values = { min: 4, max: 200 };
        let genStats = character.getRandomStats(values);
        expect(genStats).to.be.a('number');
        assert.isAtMost(values.min, genStats);
        assert.isAtLeast(values.max, genStats);
    });
    it('should return a number between the given numbers(40,20)', function () {
        let character = new Character('test2', stats.hero);
        let values = { min: 40, max: 20 };
        let genStats = character.getRandomStats(values);
        expect(genStats).to.be.a('number');
        assert.isAtMost(values.max, genStats);
        assert.isAtLeast(values.min, genStats);
    });
    it('should return a number between 0 and max(0,200)', function () {
        let character = new Character('test2', stats.hero);
        let values = { max: 200 };
        let genStats = character.getRandomStats(values);
        expect(genStats).to.be.a('number');
        assert.isAtMost(0, genStats);
        assert.isAtLeast(values.max, genStats);
    });
    it('should return a number between min and 100(20,100)', function () {
        let character = new Character('test2', stats.hero);
        let values = { min: 20 };
        let genStats = character.getRandomStats(values);
        expect(genStats).to.be.a('number');
        assert.isAtMost(values.min, genStats);
        assert.isAtLeast(100, genStats);
    });
    it('should return a number between 0 and 100(0,100)', function () {
        let character = new Character('test2', stats.hero);
        let values = {};
        let genStats = character.getRandomStats(values);
        expect(genStats).to.be.a('number');
        assert.isAtMost(0, genStats);
        assert.isAtLeast(100, genStats);
    });


    it('should return 120 or 180 damage made by 100 strength with 40 defence', function () {
        let character = new Character('test2', stats.hero);
        character.setStrength(100);
        let damage = character.criticalStrike(character, 40);
        expect(damage).to.be.oneOf([120, 180]);

    });
    it('should return 0 damage by 10 strength with 40 defence', function () {
        let character = new Character('test2', stats.hero);
        character.setStrength(10);
        let damage = character.criticalStrike(character, 40);
        expect(damage).to.be.equal(0);

    });


    it('should return 20 damage(half of 40)', function () {
        let character = new Character('test2', stats.hero);
        character.setStrength(10);
        let damage = character.resilience(character, 40);
        expect(damage).to.be.equal(20);
    });
    it('should return 0 damage(damage value is negative)', function () {
        let character = new Character('test2', stats.hero);
        character.setStrength(10);
        let damage = character.resilience(character, -40);
        expect(damage).to.be.equal(0);
    });

});